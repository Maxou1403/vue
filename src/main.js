import Vue from "vue";
import Vuesax from "vuesax";
import VueApollo from 'vue-apollo'

import App from "./App.vue";
import ApolloClient from 'apollo-boost'
import "vuesax/dist/vuesax.css";

Vue.use(Vuesax);
Vue.use(VueApollo)

Vue.config.productionTip = false;


const apolloClient = new ApolloClient({
  // You should use an absolute URL here
  uri: 'https://api-preprod.relax-max.fr/graphql'
})
const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})

new Vue({
  apolloProvider,
  render: h => h(App)
}).$mount("#app");

